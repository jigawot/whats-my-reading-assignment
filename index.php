<?php
include('data.php');
define("MINUTES", 60);
define("HOURS", 60 * MINUTES);
define("DAYS", 24 * HOURS);

$now = time() - 7 * HOURS - 29 * MINUTES + 1 * DAYS;

$day = date("n/j/Y", $now);
$time = date("G:i", $now);
$scantime = $now;

if (isset($_GET['date'])) {
    $day = $_GET['date'];
    $time = strtotime($day);
    $scantime = $time;
    $now = $scantime;
}

?>
<html>
<head>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-62658604-1', 'auto');
      ga('send', 'pageview');

    </script>
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>What's My Reading Assignment?</title>
    <style type="text/css">
        body {
            font-family: Oswald, tahoma;
            background: #596a72;
        }

        div.outer {
            background: white;
            margin: auto;
            width: 200px;
            padding: 30px;
            border-radius: 5px;
            font-size: larger;
            border-bottom: solid 2px rgba(0, 0, 0, 120);
            color: rgb(140, 140, 140);
        }

        span.date {
            display: block;
            text-align: right;
            color: rgb(140, 140, 140);
        }

        span.assignment {
            display: block;
            text-align: right;
            color: black;
        }

        div.navigation {
            text-align: center;
            margin-top: 20px;
        }

        a {
            text-decoration: none;
            color: #596a72;
            text-align: right;
            font-size: smaller;
        }

        div.footer {
            display: none;
        }

        div.top_navigation {
            font-size: 10pt;
            text-align: right;
        }
    </style>
</head>
<body>
    <div class="outer">
        <div class="top_navigation"><a href="/audio/">Audio</a></div>
        <?php
        $found = false;

        if (isset($_GET['dir']) && $_GET['dir'] == "reverse") {
            $dir = -1;
        } else {
            $dir = 1;
        }

        for ($i = 0; $i < 365 && $i > -365; $i += $dir) {
            if (isset($data[$day])) {
                $matches = array();
                $ln = false;
                if (preg_match("/D&C ([0-9]+):(([0-9]+)-[0-9]+)/", $data[$day], $matches)) {
                    $ln = "https://www.lds.org/scriptures/dc-testament/dc/${matches[1]}.${matches[2]}?lang=eng#".(intval($matches[3])-1);
                } else if (preg_match("/D&C ([0-9]+)/", $data[$day], $matches)) {
                    $ln = "https://www.lds.org/scriptures/dc-testament/dc/${matches[1]}?lang=eng";
                }


                if ($ln) {
                    $assignment = "<a href='$ln'>$data[$day]</a>";
                } else {
                    $assignment = $data[$day];
                }

                echo "<span>Reading for</span> <span class='date'>".date('l, j F', $scantime)."</span> <span class='assignment'>$assignment</span>";
                $found = true;
                break;
            } else {
                $scantime = $now + DAYS * $i;
                $day = date("n/j/Y", $scantime);
            }
        }

        if ($found) {
            $prevDay = date("n/j/Y", strtotime($day) - DAYS);
            $nextDay = date("n/j/Y", $scantime + DAYS);
        ?>

        <div class="navigation">
            <a href="?<?= "date=".$prevDay."&dir=reverse" ?>">Prev day</a> |
            <a href="?">Today</a> |
            <a href="?<?= "date=".$nextDay ?>">Next day</a>
        </div>
        <?php } else { ?>
        No reading assignment for <?=$_GET['date']?>
        <div class="navigation">
            <a href="?">Today</a>
        </div>
        <?php } ?>

        <div class="footer">v1.0</div>
    </div>
</body>
</html>
